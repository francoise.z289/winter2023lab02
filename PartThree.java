import java.util.Scanner;
public class PartThree{
	public static void main(String[]args){
		Scanner reader=new Scanner(System.in);
		
		
		//input
		System.out.println("I'm a calculator. So give me two number please. First, enter your first number.");
		double num1=reader.nextDouble();
		System.out.println("another number plz");
		double num2=reader.nextDouble();
		
		//output
		//static method
		System.out.println(num1+" + "+num2+" = "+Calculator.add(num1,num2));
		System.out.println(num1+" - "+num2+" = "+Calculator.subtract(num1,num2));
		
		//instance method
		Calculator cal=new Calculator();
		System.out.println(num1+" * "+num2+" = "+cal.multiply(num1,num2));
		System.out.println(num1+" / "+num2+" = "+cal.divide(num1,num2));
	}
}