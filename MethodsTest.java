public class MethodsTest{
	public static void main(String[]args){
		int x=5;
		//step 4e
		System.out.println("x(before calling the method): "+x);
		methodNoInputNoReturn();
		System.out.println("x(before after calling the method): "+x);
		//step 5g
		methodOneInputNoReturn(3);
		//step 5h
		System.out.println("====Step 5h====");
		System.out.println("x(before calling the method): "+x);
		methodOneInputNoReturn(x+10);
		System.out.println("x(before after calling the method): "+x);
		
		//step 6
		methodTwoInputNoReturn(3,2.5);
		
		//step 7c
		int returnTest=methodNoInputReturnInt();
		System.out.println("return test:"+returnTest);
		
		//step 8b
		double squareRoot=sumSquareRoot(9,5);
		System.out.println("squareRoot:"+squareRoot);
		
		//step 10a
		String s1="java";
		String s2="programming";
		//step 10b
		//String.length();
		//step 10c
		int leng1=s1.length();
		int leng2=s2.length();
		System.out.println(leng1+" s2: "+leng2);
		
		//11c i
		System.out.println("addOne:"+SecondClass.addOne(50));
		//System.out.println("addTwo:"+SecondClass.addTwo(50));
		
		//11 c iii
		SecondClass sc=new SecondClass();
		System.out.println("addTwo should work:"+sc.addTwo(50));
		
	}
	
	//Step4
	public static void methodNoInputNoReturn(){
		//step 4a
		System.out.println("I’m in a method that takes no input and returns nothing");
		//step 4d
		int x=20;
		System.out.println("x in the method:"+x);
		
	}
	
	//step 5
	public static void methodOneInputNoReturn(int input){
		//step 5a
		System.out.println("Inside the method one input no return");
		//step 5j
		input-=5;
		System.out.println("input: "+input);
		
		
	}
	public static void methodTwoInputNoReturn(int input1, double input2){
		System.out.println("methodTwoInputNoReturn working...");
	}
	
	//step 7a 7b
	public static int methodNoInputReturnInt(){
		return 5;
	}
	
	public static double sumSquareRoot(int num1, int num2){
		int sum=num1+num2;
		double result=Math.sqrt(sum);
		return result;
	} 
}
